import Head from 'next/head'
import Layout from '../components/layout'
import "tailwindcss/tailwind.css";
import '../styles/globals.scss'
import "@fortawesome/fontawesome-free/css/all.min.css";

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Head></Head>
      <Component {...pageProps} />
    </Layout>

  )
}

export default MyApp
