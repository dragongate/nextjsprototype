import Head from "next/head";
import Link from "next/link";

export default function Layout({ children }) {
  return (
    <>
      <Head>
        <title>Exam</title>
      </Head>
      <main>
        {children}
      </main>
    </>
  );
}
