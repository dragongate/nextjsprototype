
## Getting Started

First, run the development server:

```bash
npm i
# then
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.


## Dependencies

    - next: v10.0.8 required for API Routes and new new data fetching method.
    - react: v17.0.1 required for react hooks.
    - react-dom: v17.0.1
    - mongoose: v5.11.19 
    - swr - v0.4.2 required for state management
    - next-connect - v0.10.0 [document](https://www.npmjs.com/package/next-connect)
    - next-iron-session - v4.1.12 required for session [document](https://github.com/vvo/next-iron-session)
    - bcryptjs - v2.4.3 password-hashing library
    - validator - v13.5.2
    - multer - v1.4.2 middleware that handles multipart/form-data
    - tailwindcss - css framework
## MongoDB
    > middleware
    > models

    Models stores data schemas.

## Theme (Tailwinds Component)
    - Notus Nextjs [document](https://www.creative-tim.com/learning-lab/tailwind/nextjs/overview/notus) 